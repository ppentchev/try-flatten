//! Flatten an iterator of results of iterators...
//!
//! ...yeah, it does sound a bit confusing, does it not?
//!
//! The main intended use of this trait is to build an iterator of
//! [`Result`]-wrapped options, and be able to easily skip [`None`]
//! values without e.g. storing the whole collection into a vector
//! using `.collect()?` and then running `.flatten()` onto that.
//!
//! ```rust
//! use std::error::Error;
//!
//! use try_flatten::TryFlatten;
//!
//! const OPT_OK: [Result<Option<&str>, &str>; 3] =
//!     [Ok(Some("hello")), Ok(None), Ok(Some("goodbye"))];
//!
//! const OPT_ERR: [Result<Option<&str>, &str>; 6] = [
//!     Ok(Some("hello")),
//!     Ok(None),
//!     Ok(Some("goodbye")),
//!     Ok(None),
//!     Err("oof"),
//!     Ok(Some("never")),
//! ];
//!
//! # fn main() -> Result<(), &'static str> {
//! let data: Vec<&str> = OPT_OK.into_iter().try_flatten().collect::<Result<_, _>>()?;
//! println!("{} results collected successfully", data.len());
//! let res: Result<Vec<&str>, &'static str> = OPT_ERR.into_iter().try_flatten().collect::<Result<_, _>>();
//! println!("This should be an error: {:?}", res);
//! # Ok(())
//! # }
//! ```
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
// Activate most of the clippy::restriction lints that we have come across...
#![warn(clippy::exhaustive_enums)]
#![warn(clippy::missing_docs_in_private_items)]
#![warn(clippy::missing_inline_in_public_items)]
#![warn(clippy::panic)]
#![warn(clippy::pattern_type_mismatch)]
#![warn(clippy::shadow_reuse)]
#![warn(clippy::shadow_same)]
#![warn(clippy::str_to_string)]
// ...except for these ones.
#![allow(clippy::implicit_return)]
// Activate most of the clippy::pedantic lints that we have come across...
#![warn(clippy::explicit_into_iter_loop)]
#![warn(clippy::match_bool)]
#![warn(clippy::missing_errors_doc)]
#![warn(clippy::panic_in_result_fn)]
#![warn(clippy::too_many_lines)]
#![warn(clippy::unnecessary_wraps)]
#![warn(clippy::unreachable)]
// ...except for these ones.
#![allow(clippy::module_name_repetitions)]
// Activate most of the clippy::nursery lints that we have come across...
#![warn(clippy::branches_sharing_code)]
#![warn(clippy::missing_const_for_fn)]

use std::iter;

/// An implementation of [`TryFlatten::try_flatten`].
pub struct TryFlattenImpl<T, E, IRI, II, I>
where
    IRI: Iterator<Item = Result<II, E>>,
    II: IntoIterator<Item = T, IntoIter = I>,
    I: Iterator<Item = T>,
{
    /// The iterator to wrap.
    it: IRI,
    /// Can we run .next() on .inner?
    ready: bool,
    /// The iterator we fetched from the iterator to wrap.
    inner: Box<dyn Iterator<Item = T>>,
}

impl<T, E, IRI, II, I> Iterator for TryFlattenImpl<T, E, IRI, II, I>
where
    IRI: Iterator<Item = Result<II, E>>,
    II: IntoIterator<Item = T, IntoIter = I>,
    I: Iterator<Item = T> + 'static,
    T: 'static,
{
    type Item = Result<T, E>;

    // Can a recursive method even *be* inlined?
    #[allow(clippy::missing_inline_in_public_items)]
    fn next(&mut self) -> Option<Self::Item> {
        if self.ready {
            match self.inner.next() {
                Some(value) => Some(Ok(value)),
                None => {
                    self.ready = false;
                    self.next()
                }
            }
        } else {
            self.it.next().and_then(|res| match res {
                Ok(inner) => {
                    self.inner = Box::new(inner.into_iter());
                    self.ready = true;
                    self.next()
                }
                Err(err) => Some(Err(err)),
            })
        }
    }
}

/// Flatten an iterator of Result-wrapped iterators.
pub trait TryFlatten<T, E, IRI, II, I>: Iterator
where
    IRI: Iterator<Item = Result<II, E>>,
    II: IntoIterator<Item = T, IntoIter = I>,
    I: Iterator<Item = T>,
{
    /// Wrap an iterator, fail on errors, skip None values.
    fn try_flatten(self) -> TryFlattenImpl<T, E, IRI, II, I>;
}

impl<T, E, IRI, II, I> TryFlatten<T, E, IRI, II, I> for IRI
where
    IRI: Iterator<Item = Result<II, E>>,
    II: IntoIterator<Item = T, IntoIter = I>,
    I: Iterator<Item = T>,
    T: 'static,
{
    #[inline]
    fn try_flatten(self) -> TryFlattenImpl<T, E, IRI, II, I>
    where
        IRI: Iterator<Item = Result<II, E>>,
        II: IntoIterator<Item = T, IntoIter = I>,
        I: Iterator<Item = T>,
    {
        TryFlattenImpl {
            it: self,
            ready: false,
            inner: Box::new(iter::empty()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::TryFlatten;

    const OPT_OK: [Result<Option<&str>, &str>; 3] =
        [Ok(Some("hello")), Ok(None), Ok(Some("goodbye"))];
    const OPT_ERR: [Result<Option<&str>, &str>; 6] = [
        Ok(Some("hello")),
        Ok(None),
        Ok(Some("goodbye")),
        Ok(None),
        Err("oof"),
        Ok(Some("never")),
    ];

    #[test]
    fn test_ok_option() {
        let mut it = OPT_OK.into_iter().try_flatten();
        assert_eq!(it.next(), Some(Ok("hello")));
        assert_eq!(it.next(), Some(Ok("goodbye")));
        assert_eq!(it.next(), None)
    }

    #[test]
    fn test_err_option() {
        let mut it = OPT_ERR.into_iter().try_flatten();
        assert_eq!(it.next(), Some(Ok("hello")));
        assert_eq!(it.next(), Some(Ok("goodbye")));
        assert_eq!(it.next(), Some(Err("oof")));
        assert_eq!(it.next(), Some(Ok("never")));
        assert_eq!(it.next(), None)
    }

    #[test]
    fn test_ok_option_collect() {
        let res: Result<Vec<_>, _> = OPT_OK.into_iter().try_flatten().collect();
        assert_eq!(res, Ok(vec!["hello", "goodbye"]));
    }

    #[test]
    fn test_err_option_collect() {
        let res: Result<Vec<_>, _> = OPT_ERR.into_iter().try_flatten().collect();
        assert_eq!(res, Err("oof"));
    }

    #[derive(Debug, Default)]
    struct LazyTest {
        count: usize,
    }

    impl Iterator for LazyTest {
        type Item = Result<Vec<u32>, String>;

        fn next(&mut self) -> Option<Self::Item> {
            self.count += 1;
            match self.count {
                1 => Some(Ok(vec![1, 2])),
                2 => Some(Ok(vec![])),
                3 => Some(Ok(vec![3])),
                4 => Some(Err("oof".to_owned())),
                _ => panic!("how did we get here?"),
            }
        }
    }

    #[test]
    fn test_lazy() {
        let mut it_values = LazyTest::default().try_flatten();
        assert_eq!(it_values.next(), Some(Ok(1)));
        assert_eq!(it_values.next(), Some(Ok(2)));
        assert_eq!(it_values.next(), Some(Ok(3)));
        assert_eq!(it_values.next(), Some(Err("oof".to_owned())));

        let it_collect = LazyTest::default().try_flatten();
        let res = it_collect.collect::<Result<Vec<_>, _>>();
        assert_eq!(res, Err("oof".to_owned()));
    }
}
